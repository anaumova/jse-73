package ru.tsc.anaumova.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.tsc.anaumova.tm.config.ApplicationConfiguration;
import ru.tsc.anaumova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.anaumova.tm.exception.field.EmptyIdException;
import ru.tsc.anaumova.tm.exception.field.EmptyNameException;
import ru.tsc.anaumova.tm.exception.field.EmptyUserIdException;
import ru.tsc.anaumova.tm.marker.UnitCategory;
import ru.tsc.anaumova.tm.model.Project;
import ru.tsc.anaumova.tm.service.ProjectService;
import ru.tsc.anaumova.tm.service.UserService;
import ru.tsc.anaumova.tm.util.UserUtil;

import java.util.List;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class ProjectServiceTest {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @NotNull
    @Autowired
    private UserService userService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private final Project project = new Project("test");

    @NotNull
    private static String USER_ID;

    @Before
    public void init() {
        userService.createUser("test", "test", null);
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        projectService.save(project, USER_ID);
    }

    @After
    public void destroy() {
        projectService.clearByUserId(USER_ID);
    }

    @Test
    public void add() {
        Assert.assertThrows(EmptyUserIdException.class, () -> projectService.add("test", null));
        Assert.assertThrows(EmptyNameException.class, () -> projectService.add(null, USER_ID));
        projectService.add("test", USER_ID);
        Assert.assertEquals(2, projectService.countByUserId(USER_ID));
    }

    @Test
    public void save() {
        Assert.assertThrows(EmptyUserIdException.class, () -> projectService.save(new Project(), null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.save(null, USER_ID));
        projectService.save(new Project(), USER_ID);
        Assert.assertEquals(2, projectService.countByUserId(USER_ID));
    }

    @Test
    public void remove() {
        Assert.assertThrows(EmptyUserIdException.class, () -> projectService.remove(project, null));
        projectService.remove(project, USER_ID);
        Assert.assertEquals(0, projectService.countByUserId(USER_ID));
    }

    @Test
    public void removeByIdAndUserId() {
        @NotNull final String projectId = project.getId();
        Assert.assertThrows(EmptyUserIdException.class, () -> projectService.removeByIdAndUserId(projectId, null));
        Assert.assertThrows(EmptyIdException.class, () -> projectService.removeByIdAndUserId(null, USER_ID));
        projectService.removeByIdAndUserId(projectId, USER_ID);
        Assert.assertEquals(0, projectService.countByUserId(USER_ID));
    }

    @Test
    public void findAllByUserId() {
        Assert.assertThrows(EmptyUserIdException.class, () -> projectService.findAllByUserId(null));
        List<Project> projects = projectService.findAllByUserId(USER_ID);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findByIdAndUserId() {
        @NotNull final String projectId = project.getId();
        Assert.assertThrows(EmptyUserIdException.class, () -> projectService.findByIdAndUserId(projectId, null));
        Assert.assertThrows(EmptyIdException.class, () -> projectService.findByIdAndUserId(null, USER_ID));
        Project projectFromDB = projectService.findByIdAndUserId(projectId, USER_ID);
        Assert.assertNotNull(project);
        Assert.assertEquals(projectId, projectFromDB.getId());
    }

    @Test
    public void existsByIdAndUserId() {
        @NotNull final String projectId = project.getId();
        Assert.assertThrows(EmptyUserIdException.class, () -> projectService.existsByIdAndUserId(projectId, null));
        Assert.assertThrows(EmptyIdException.class, () -> projectService.existsByIdAndUserId(null, USER_ID));
        boolean exists = projectService.existsByIdAndUserId(projectId, USER_ID);
        Assert.assertTrue(exists);
    }

    @Test
    public void clearByUserId() {
        Assert.assertThrows(EmptyUserIdException.class, () -> projectService.clearByUserId(null));
        projectService.clearByUserId(USER_ID);
        Assert.assertEquals(0, projectService.countByUserId(USER_ID));
    }

    @Test
    public void countByUserId() {
        Assert.assertThrows(EmptyUserIdException.class, () -> projectService.countByUserId(null));
        long count = projectService.countByUserId(USER_ID);
        Assert.assertEquals(1L, count);
    }

}