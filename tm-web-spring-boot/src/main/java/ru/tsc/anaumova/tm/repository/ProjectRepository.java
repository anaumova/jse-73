package ru.tsc.anaumova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.anaumova.tm.model.Project;

import java.util.List;
import java.util.Optional;

public interface ProjectRepository extends JpaRepository<Project, String> {

    @NotNull
    Optional<Project> findByIdAndUserId(@NotNull String id, @NotNull String userId);

    @NotNull
    List<Project> findAllByUserId(@NotNull String userId);

    long countByUserId(@NotNull String userId);

    boolean existsByIdAndUserId(@NotNull String id, @NotNull String userId);

    void deleteByIdAndUserId(@NotNull String userId, String id);

    void deleteAllByUserId(@NotNull String userId);

}