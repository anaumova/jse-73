package ru.tsc.anaumova.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.tsc.anaumova.tm.config.ApplicationConfiguration;
import ru.tsc.anaumova.tm.exception.field.EmptyLoginException;
import ru.tsc.anaumova.tm.exception.field.EmptyPasswordException;
import ru.tsc.anaumova.tm.marker.UnitCategory;
import ru.tsc.anaumova.tm.service.UserService;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class UserServiceTest {

    @NotNull
    @Autowired
    private UserService userService;

    @Test
    public void createUser() {
        Assert.assertThrows(EmptyLoginException.class, () -> userService.createUser("", "12345", null));
        Assert.assertThrows(EmptyPasswordException.class, () -> userService.createUser("test", "", null));
        userService.createUser("user", "user", null);
        Assert.assertNotNull(userService.findByLogin("user"));
    }

    @Test
    public void findByLogin() {
        userService.createUser("user", "user", null);
        Assert.assertThrows(EmptyLoginException.class, () -> userService.findByLogin(null));
        Assert.assertNotNull(userService.findByLogin("user"));
    }

}